package com.cource.dollar.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "ValCurs")
@XmlAccessorType(XmlAccessType.FIELD)
public class Valcurs {

    @XmlElement(name="Valute")
    private List<Valute> valcurs;

    public List<Valute> getValcurs() {
        return valcurs;
    }

    public void setValcurs(List<Valute> valcurs) {
        this.valcurs = valcurs;
    }
}
