package com.cource.dollar.controller;

import com.cource.dollar.model.Valcurs;
import com.cource.dollar.model.Valute;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/api/example")
public class RestExchangeExample {

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/value")
    public String getValue() {
        Valcurs valcurs = restTemplate.getForObject("http://www.cbr.ru/scripts/XML_daily.asp", Valcurs.class);
        if (valcurs != null) {
            for (Valute valute : valcurs.getValcurs()) {
                if (valute.getCharCode().equals("HKD")) {
                    return valute.getValue();
                }
            }
        }
        return null;
    }

}
